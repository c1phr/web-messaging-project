<!DOCTYPE html>
<?php
$login_labels = array("userid" => "UserID", "password" => "Password");
session_start();

if ($_SESSION[failed]) {
    $failed = true;
    unset($_SESSION[failed]);
}

if (isset($_SESSION['username'])) {
    header('Location: secure_index.php');
}
?>

    <head>
    <title>Ryan Batchelder Sign In</title>
    <link rel="stylesheet" type="text/css" href="styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
    </head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Ryan Batchelder</div>
    &nbsp;
    <hr class="partial_rule" />
    <?php
        if ($failed) //If the user gets here on a failed login, tell them so. I chose to send failed logins back here if all of the fields are filled out but with incorrect information.
            echo "<p id='main_text'>Login failed, please try again. <a href='registration.php'>Click here to register.</a></p>";
        else
            echo "<p id='main_text'>If you would like to see the rest of the site, please log in. <a href='registration.php'>Click here to register.</a></p>";
    ?>
    <form class="login" action="php/loginValidator.php" method="post">
        <?php
            foreach($login_labels as $field => $label) { //Display the login fields.
                if ($field == "userid")
                    echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='100' /></div>";
                else if ($field == "password")
                    echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='password' name='$field' id='$field' size='30' maxlength='100' /></div>";
            }
        ?>
        <div><input class="submit_left" type="submit" value="Login" /></div>
    </form>
    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>
</html>