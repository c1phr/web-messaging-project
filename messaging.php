<!DOCTYPE html>
<?php
session_start();

if (!isset($_SESSION[userid])) {
    header('Location: index.php');
}

if(isset($_SESSION[reply_to])) { //Clean up in the event that the user doesn't follow through with a reply.
    unset($_SESSION[reply_to]);
    unset($_SESSION[reply_subject]);
}

include('php/database_info.inc');
?>

<head>
    <title>Messaging</title>
    <link rel="stylesheet" type="text/css" href="styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Messaging</div>
    <hr class="partial_rule" />
    <p id="main_text">Welcome to the messaging system! Click on any message to read the message content and send replies. Unread messages are listed in bold.</p>
    <?php
    $message_box = mysql_query("SELECT * FROM messaging WHERE recipient = '". mysql_real_escape_string($_SESSION[userid]) ."' ORDER BY id DESC"); //Get all the messages from the database where the user is the recipient, and get them in descending order by ID (which is newest first by nature of ID).
    if ($message_box) {
        echo "<div class='message_table'>";
        echo "<table class='messages'>
            <tr class='table_labels'>
                <td class='message_table_small'>Time</td>
                <td class='message_table_small'>From</td>
                <td class='message_table_large'>Subject</td>
            </tr>";
        while ($row = mysql_fetch_array($message_box)) { //Go through the array of the messages and dump them out to the screen.
            if ($row[status] == "unread") { //If a message's status is unread, bold it on display.
                echo "<tr class='bold'>";
                echo "<td class='message_table_small'><a href='php/readmessage.php?id={$row[id]}'>" . $row[time] . "</a></td><td class='message_table_small'><a href='php/readmessage.php?id={$row[id]}'>" . $row[sender] . "</a></td><td class='message_table_large'><a href='php/readmessage.php?id={$row[id]}'>" . $row[subject] . "</a></td>";
            }
            else { //Otherwise, just display it as normal
                echo "<tr>";
                echo "<td class='message_table_small'><a href='php/readmessage.php?id={$row[id]}'>" . $row[time] . "</a></td><td class='message_table_small'><a href='php/readmessage.php?id={$row[id]}'>" . $row[sender] . "</a></td><td class='message_table_large'><a href='php/readmessage.php?id={$row[id]}'>" . $row[subject] . "</a></td>";
            }
            echo "</tr>";
        }
       echo "</table></div>";
    }
    ?>
    <p id="main_text"><a href="secure_index.php"><-- Go back</a> | <a href="php/sendmessage.php">Send Message --></a></p>
    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>
</html>