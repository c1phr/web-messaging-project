<!DOCTYPE html>
<?php
session_start();

if (!isset($_SESSION[userid])) {
    header('Location: index.php');
}

$edit_labels = array("email" => "Email Address", "password" => "Password");
?>

<head>
    <title>Edit Your Account</title>
    <link rel="stylesheet" type="text/css" href="styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Edit Your Account</div>
    <hr class="partial_rule" />
    <p id="main_text">Enter the information you would like to change in the fields below. Blank fields will remain unchanged.</p>
    <form class="login" action="php/userEditValidator.php" method="post">
        <?php
        if ($_SESSION[fixEmail]) {
            echo "<p>That email address was invalid. Please try again.</p>";
            unset($_SESSION[fixEmail]);
        }
        foreach($edit_labels as $field => $label) { //Display all of the registration fields
            if ($field == "email")
                echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='45' /></div>";
            else if ($field == "password")
                echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='password' name='$field' id='$field' size='30' maxlength='100' /></div>";
        }
        echo "<p id='main_text'><a href='php/deleteConfirm.php'>Click here to delete your account</a></p>";
        ?>
        <div><input class="submit_left" type="submit" value="Submit Edits" /></div>
    </form>
    <p id="main_text"><a href="secure_index.php"><-- Cancel edits and go back</a></p>
    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>