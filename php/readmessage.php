<!DOCTYPE html>
<?php
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();
include('database_info.inc');
$id = $_GET[id];
?>

<head>
    <title>Read Message</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Read Message</div>
    &nbsp;
    <hr class="partial_rule" />

    <div id="main_text">
    <?php
        $get_message = mysql_query("SELECT * FROM messaging WHERE id = '". $id ."'"); //Get the message from the database.
        mysql_query("UPDATE messaging SET `status` = 'read' WHERE id = '" . $id . "'"); //Set the message as read, so it won't be bolded anymore.
        if ($get_message) {
            $message = mysql_fetch_array($get_message);
            if ($message[recipient] != $_SESSION[userid]) { //Make sure they didn't try to change the id in the url bar. They can only do that for messages that were sent to them.
                echo "Nice try, but this message isn't for you <br />";
                echo "<a href='../messaging.php'><-- Go Back</a>";
            }
            else { //Display the message if it is indeed intended for the user that opened it.
                $_SESSION[reply_to] = $message[sender];
                $_SESSION[reply_subject] = "Re: " . $message[subject];
                echo "<span class='underline'>From:</span> " . $message[sender] . "<br />";
                echo "<span class='underline'>Time:</span> " . $message[time] . "<br />";
                echo "<span class='underline'>Subject:</span> " . $message[subject] . "<br />";
                echo "<span class='underline'>Message:</span> <br />" . $message[body] . "<br />";
                echo "<a href='../messaging.php'><-- Go Back</a> | <a href='delete_message.php?id={$id}'>Delete Message</a> | <a href='sendmessage.php'>Reply --></a>";
            }
        }
        else {
           echo "An error has occured!<br /><a href='../messaging.php'><-- Go Back</a>";
        }
    ?>
    </div>

    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>