<!DOCTYPE html>
<?php
$reg_labels = array("email" => "Email Address", "userid" => "UserID", "password" => "Password");
$reg_success = true;
$check_regex = "/(.com|.org|.gov|.edu|.net)$/"; //Regex to check for the correct email domains
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();
/*if (isset($_SESSION['username'])) {
    header('Location: secure_index.php');
}*/
include('database_info.inc');
?>

<head>
    <title>Ryan Batchelder Registration</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Ryan Batchelder</div>
    &nbsp;
    <hr class="partial_rule" />

    <?php
    foreach ($_POST as $field => $value) { //Check to make sure they didn't leave any fields empty
        if (empty($value)) {
            echo "<div id='main_text'>You must fill in the $reg_labels[$field] field.<br /></div>";
            $reg_success = false;
        }
        if ($field == "email") {
            if (!preg_match($check_regex, $value)) { //Catch if they put in an email in the wrong format
                echo "<div id='main_text'>Your email address doesn't appear to be valid, please try again. <br /></div>";
                $reg_success = False;
            }
        }
    }

    //The next two if statements check to see if the username or email already exist in the database.
        if (mysql_num_rows(mysql_query("SELECT * FROM user WHERE (username = '" . mysql_real_escape_string($_POST[userid]) . "')")) > 0) {
            $reg_success = false;
            echo "<div id='main_text'>That username is already taken</div>";
        }
        if (mysql_num_rows(mysql_query("SELECT * FROM user WHERE (email = '" . mysql_real_escape_string($_POST[email]) . "')")) > 0) {
            $reg_success = false;
            echo "<div id='main_text'>An account with that email address has already been created</div>";
        }
    if (!$reg_success) { //If anything above failed, redisplay the form.
        echo "<form class='login' action='registrationValidator.php' method='post'>";
        foreach($reg_labels as $field => $label) {
            if ($field == "email")
                echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='45' /></div>";
            else if ($field == "userid")
                echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='100' /></div>";
            else if ($field == "password")
                echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='password' name='$field' id='$field' size='30' maxlength='100' /></div>";
        }
        echo "<div><input class='submit_left' type='submit' value='Login' /></div>";
        echo "</form>";
    }
    else { //If registration was successful, then add all the information to the proper databases.
        $password = md5($_POST[password]); //It's not secure, but MD5 the passwords anyway to discourage script kiddie hacking
        mysql_query("INSERT INTO user (email, username, password) VALUES ('$_POST[email]', '$_POST[userid]', '$password')"); //Insert the registration data into the user database
        mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_POST[userid]', '$time', 'Registration')"); //Log the new user registration
        session_start(); //Start a session so that the user doesn't need to log in again
        $_SESSION[userid] = $_POST[userid]; //Set the username in the session to the newly created account
        $_SESSION[newAcc] = true; //We're going to use this to change how we welcome the new user to their account page.
        echo "<a href='../secure_index.php'>Click here if your browser does not redirect you.</a>";
        header('location: ../secure_index.php');
    }
    ?>

    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>