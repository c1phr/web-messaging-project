<!DOCTYPE html>
<?php
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();
include('database_info.inc');
$id = $_GET[id];
?>

<head>
    <title>Delete Message</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Delete Message</div>
    &nbsp;
    <hr class="partial_rule" />

    <div id="main_text">
        <?php
        $get_message = mysql_query("SELECT * FROM messaging WHERE id = '". $id ."'"); //Get the message from the database
        if ($get_message) {
            $message = mysql_fetch_array($get_message);
            if ($message[recipient] != $_SESSION[userid]) { //Make sure they didn't try to change the id in the url bar. They can only do that for messages that were sent to them.
                echo "Nice try, but this message isn't for you <br />";
                echo "<a href='../messaging.php'><-- Go Back</a>";
            }
            else {
                mysql_query("DELETE FROM messaging WHERE id = '" . $id ."'") or die("An error occurred when trying to delete your message"); //Delete the message from the database.
                echo "Message deleted successfully.";
                echo "<a href='../messaging.php'><-- Go Back</a>";
            }
        }
        else {
            echo "An error has occured!<br /><a href='../messaging.php'><-- Go Back</a>";
        }
        ?>
    </div>

    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>