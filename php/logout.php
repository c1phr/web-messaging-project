<?php
session_start();
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');

include('database_info.inc'); //Include the file that handles database logins
mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_SESSION[userid]', '$time', 'Log Out')"); //Record the logout in the log table

unset($_SESSION[userid]); //Remove the userid from the session variable, for completeness
session_destroy(); //Destroy the user's session.

header("Location: ../index.php");

?>