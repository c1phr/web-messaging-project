<!DOCTYPE html>
<?php
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();
if (isset($_SESSION['username'])) {
    header('Location: secure_index.php');
}
include('database_info.inc');
$users = '';
$user_get = mysql_query("SELECT username FROM user");
while ($row = mysql_fetch_array($user_get)) { //Build a dropdown list with all the registered users.
    if ($row[username] != $_SESSION[userid]) { //Users shouldn't really need to send messages to themselves.
        if ($row[username] == $_POST[to]) {
            $users .= "<option selected>" . $row[username] . "</option>"; //If they got back here because their message failed to send, at least fill this in correctly for them
        }
        else {
            $users .= "<option>" . $row[username] . "</option>";
        }
    }
}
$message_complete = True;
?>

<head>
    <title>Send Message</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <?php
    if (isset($_SESSION[reply_to])) { //Change the title bar based on whether the user is replying to a message or sending a new one.
        echo "<div id='header'>Send Reply</div>";
    }
    else {
        echo "<div id='header'>Send Message</div>";
    }
    ?>
    &nbsp;
    <hr class="partial_rule" />

    <div id="main_text">
        <?php
        foreach ($_POST as $field => $value) { //Validate to make sure the user filled in all the fields.
            if (empty($value)) {
                echo "You must fill in the '$field' field.'<br />";
                $message_complete = False; //Nope, they didn't do it right
            }
            if ($field == "message_body") {
                if (empty($value) or $value == "Write the body of your message here") {
                    echo "You must write a message in the 'Body' field.<br />";
                    $message_complete = False; //Nope, they didn't do it right
                }
            }
        }

        if (!$message_complete) {
            echo "<form id='form_space' action='message_validator.php' method='post'>";
            echo "<label for='to'>To: </label><select width='55' class='form_right users' name='to'>" . $users . "</select>";
            echo "<label for='subject'>Subject: </label><input class='form_right' type='text' name='subject' size='55' maxlength='100' value='$_POST[subject]' /> ";
            echo "<label for='body'>Body: </label><textarea class='form_right' name='message_body' rows=10 cols=53>$_POST[message_body]</textarea> ";
            echo "<div><input class='button_clear' type='submit' value='Send Message' /></div>"; //Submit button
            echo "<div style='clear: both;'><a href='../messaging.php'><-- Go Back</a></div>";
        }

        else { //Yep, they did it right
            $get_id = mysql_fetch_array(mysql_query("SELECT * FROM messaging ORDER BY id DESC LIMIT 1")); //Get the message with the largest ID so we can increment it by one
            $id = $get_id[id] + 1;
            mysql_query("INSERT INTO messaging (id, recipient, sender, time, status, subject, body) VALUES ('$id', '$_POST[to]', '$_SESSION[userid]', '$time', 'unread', '$_POST[subject]', '$_POST[message_body]')") or die("'$id', '$_POST[to]', '$_SESSION[userid]', '$time', 'unread', '$_POST[subject]', '$_POST[message_body]'");
            if(isset($_SESSION[reply_to])) { //Clean up if the message that was sent was a reply.
                unset($_SESSION[reply_to]);
                unset($_SESSION[reply_subject]);
            }
            echo "Your message has been sent successfully<br /><a href='../messaging.php'>Click here to go back</a>";
        }
        ?>
    </div>

    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>