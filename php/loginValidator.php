<!DOCTYPE html>
<?php
$login_labels = array("userid" => "UserID", "password" => "Password");
$login_success = true;
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();
if (isset($_SESSION[userid])) {
    header('Location: secure_index.php');
}
include('database_info.inc');
?>

<head>
    <title>Ryan Batchelder Sign In</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Ryan Batchelder</div>
    &nbsp;
    <hr class="partial_rule" />

    <?php
    foreach ($_POST as $field => $value) { //If they forgot to fill in fields, give them an opportunity to fix that. We won't log this as a failed login.
        if (empty($value)) {
            echo "<div id='main_text'>You must fill in the $login_labels[$field] field.<br /></div>";
            $login_success = false;
        }
    }
    if (!$login_success) { //Redisplay the fields if the user forgot stuff.
        echo "<form class='login' action='loginValidator.php' method='post'>";
        foreach($login_labels as $field => $label) {
            if ($field == "userid")
                echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='100' /></div>";
            else if ($field == "password")
                echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='password' name='$field' id='$field' size='30' maxlength='100' /></div>";
        }
        echo "<div><input class='submit_left' type='submit' value='Login' /></div>";
        echo "</form>";
    }
    else { //All the fields were filled in.
        $login = mysql_query("SELECT * FROM user WHERE (username = '" . mysql_real_escape_string($_POST[userid]) . "') and (password = '" . mysql_real_escape_string(md5($_POST[password])) . "')");
        if (mysql_num_rows($login) == 1) { //Check to see if the SQL Query came back with the user.
            $_SESSION[userid] = $_POST[userid]; //Set the session userid so that they can be logged in
            mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_POST[userid]', '$time', 'Log In')"); //Record login in the log
            echo "<a href='../secure_index.php'>Click here if your browser does not redirect you.</a>";
            header('location: ../secure_index.php');
        }
        else { //If the user populated all fields but their login wasn't found
            mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_POST[userid]', '$time', 'Failed Attempt')"); //Log the failed attempt
            $_SESSION[failed] = true; //Set a session variable so we can tell them back on the initial login screen that their attempt failed
            header('location: ../index.php'); //Finally, send them back to the login screen.
        }
    }
    ?>

    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>