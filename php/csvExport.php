<?php
$time = date('m/d/y H:i');
$dumpFile = fopen("php://output", "w");
session_start();

if (!isset($_SESSION[userid])) {
    header('Location: ../index.php');
    if ($_SESSION[userid] != 'admin')
        header('Location: ../secure_index.php');
}
include('database_info.inc');

$dumpFile = fopen('php://output', 'w'); //This opens a the standard output for writing.

$logQuery = "SELECT * FROM logbook"; //Same query as before. This probably could have been passed over as a SESSION variable and multiple database queries could have been avoided...
$logContents = mysql_query($logQuery); //...but this site isn't too big, and this is a bit more clear.

$headers = array("Username", "Time", "Type", "Account Active"); //Set up the table headers to be dumped into the CSV

header('Content-Type: text/csv'); //Setup the file headers for the CSV
header('Content-Disposition: attachment; filename="output.csv"');
header('Pragma: no-cache');
header('Expires: 0');
fputcsv($dumpFile, $headers); //Throw in all those headers that we setup before

mysql_query("INSERT INTO logbook (username, time, type, accountActive) VALUES ('$_SESSION[userid]', '$time', 'Log Downloaded')"); //Log that the log was downloaded

while ($row = mysql_fetch_array($logContents, MYSQL_ASSOC)) { //MYSQL_ASSOC gets rid of a terrible duplication issue that caused the CSV to have two of ever entry
    fputcsv($dumpFile, array_values($row)); //Dump in all of the log rows into the CSV.
}

?>