<?php
session_start();
include('database_info.inc');
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');

if (!isset($_SESSION[userid]))
    header('Location: index.php');

mysql_query("DELETE FROM user WHERE username = '" . mysql_real_escape_string($_SESSION[userid]) . "'"); //Delete the user from the user table so they can't log in anymore
mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_SESSION[userid]', '$time', 'Account Deleted')"); //Log the deleted user
mysql_query("UPDATE logbook SET `accountActive` = 'FALSE' WHERE username = '" . mysql_real_escape_string($_SESSION[userid]) . "'"); //Find all of the entries in the log for the user and flag them as inactive
unset($_SESSION[userid]);
session_destroy();
header("Location: ../index.php");
?>