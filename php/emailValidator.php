<!DOCTYPE html>
<head>
    <?php
    $form_labels = array("name" => "Name", "email" => "Email Address", "subject" => "Email Subject", "type" => "Email Type", "body" => "Email Body"); //Array of form elements
    $check_regex = "/(.com|.org|.gov|.edu|.net)$/"; //Regex to check for the correct email domains
    $form_completed = True;
    $my_email = "ryan@nau.edu";
    date_default_timezone_set('America/Phoenix'); //Not sure if this is needed, but just in case.
    $timestamp = date('m/d/y h:i a');
    ?>
    <title>Email Ryan</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container">
    <div id="header">Email Ryan</div>
    <div>
        <ul id="nav">
            <li><a href="../index.html" class="left_end">Project 1</a></li>
            <li><a href="../page2.html">Project 2</a></li>
            <li><a href="../page3.html">Project 3</a></li>
            <li><a href="../email_form_php.php" class="right_end">Project 5/6</a></li>
        </ul>
    </div>
    <hr class="partial_rule" />
    <div id="main_text">
            <?php

            foreach ($_POST as $field => $value) { //Iterate through the elements in the POST array
                if ($field == "name" or $field == "subject" or $field == "type") {
                    if(empty($value)) {
                        echo "You must fill in the '$form_labels[$field]' field.<br />"; //Let the user know what they forgot to fill in
                        $form_completed = False;
                    }
                }

                if ($field == "email") {
                    if(empty($value)) {
                        echo "You must fill in the '$form_labels[$field]' field. <br />"; //Catch if they forgot to put in their email address altogether
                        $form_completed = False;
                    }
                    elseif(!preg_match($check_regex, $value)) { //Catch if they put in an email in the wrong format
                        echo "Your email address doesn't appear to be valid, please try again. <br />";
                        $form_completed = False;
                    }
                }

                if ($field == "email_body") {
                    if(empty($value) or $value == "Write the body of your message here") { //Catch both when the user submits an empty email body or leaves the default text in
                        echo "You must write a message in the 'Email Body' field. <br />";
                        $form_completed = False;
                    }
                }
            }

            if ($form_completed == False) {
                echo "<form id='form_space' action='../php/emailValidator.php' method='post'>";
            /*Form is redisplayed by the code below*/
                foreach ($form_labels as $field => $label) { //Iterate through the and echo out labels again
                    if ($field != "type") {
                        echo "<div><label for='$label'>$label: </label> ";
                    }
                    if ($field == "name" or $field == "email" or $field == "subject") { //Same code as the original form, except we're going to put in the previously entered values.
                        echo "<input class='form_right' type='text' name='$field' id='$field' size='55' maxlength='100' value='$_POST[$field]' /> ";
                    }
                    elseif ($field == "type") { //A bunch of cases to activate the radio button that was previously checked.
                        echo "<div class='radio_fix form_right'><select name='email_type'>";
                        if ($_POST[email_type] == "Urgent")
                            echo "<option value='Urgent' selected>Urgent</option>";
                        else
                            echo "<option value='Urgent'>Urgent</option>";
                        if ($_POST[email_type] == "Question")
                            echo "<option value='Question' selected>Question</option>";
                        else
                            echo "<option value='Question'>Question</option>";
                        if ($_POST[email_type] == "Comment")
                            echo "<option value='Comment' selected>Comment</option>";
                        else
                            echo "<option value='Comment'>Comment</option>";
                        echo "</select><br />";

                        //The following is the old code that managed radio buttons instead of a dropdown.

                        /*if ($_POST[group1] == 'Urgent') {
                                echo "<div class='radio_fix'><span class='radio_space'>Urgent:<input type='radio' value='Urgent' name='group1' checked /></span>
                                      <span class='radio_space'>Question:<input type='radio' value='Question' name='group1' /></span>
                                      <span class='radio_space'>Comment:<input type='radio' value='Comment' name='group1' /></span><br />";
                        }
                        elseif ($_POST[group1] == 'Question') {
                            echo "<div class='radio_fix'><span class='radio_space'>Urgent:<input type='radio' value='Urgent' name='group1' /></span>
                                  <span class='radio_space'>Question:<input type='radio' value='Question' name='group1' checked /></span>
                                  <span class='radio_space'>Comment:<input type='radio' value='Comment' name='group1' /></span><br />";

                        }
                        elseif ($_POST[group1] == 'Comment') {
                            echo "<div class='radio_fix'><span class='radio_space'>Urgent:<input type='radio' value='Urgent' name='group1' /></span>
                                  <span class='radio_space'>Question:<input type='radio' value='Question' name='group1' /></span>
                                  <span class='radio_space'>Comment:<input type='radio' value='Comment' name='group1' checked /></span><br />";
                        }
                        else { //Catches the default case when no radio is selected (when the form is first loaded)
                            echo "<div class='radio_fix'><span class='radio_space'>Urgent:<input type='radio' value='Urgent' name='group1' /></span>
                                  <span class='radio_space'>Question:<input type='radio' value='Question' name='group1' /></span>
                                  <span class='radio_space'>Comment:<input type='radio' value='Comment' name='group1' /></span><br />";
                        }*/
                    }
                    elseif ($field == "body") {
                        echo "<textarea class='form_right' name='email_body' rows=10 cols=50>$value</textarea>";
                    }

                    echo "</div>"; //Close the div with each label/input pair.

                }
                echo "<div><input class='button_clear' type='submit' value='Send Email' /></div></form>"; //Echo out the submit button.
            }
            else {
                $to_send = "Time: " . $timestamp . "\nName: " . $_POST[name] . "\nReturn Address: " . $_POST[email] . "\nEmail Type: " . $_POST[email_type] . "\nEmail Body:\n" . $_POST[email_body];
                $extra_headers = "From: " . $my_email . "\r\n" . "CC: " . $_POST[email];
                if (mail($my_email, $_POST[subject], $to_send, $extra_headers)) {
                echo "<div><p>Thanks for reaching out! I'll give a response as soon as possible!</p>
                        <p><span class='summary'>Summary</span><br />
                        Time: $timestamp<br />
                        Name: $_POST[name]<br />
                        Email Address: $_POST[email]<br />
                        Subject: $_POST[subject]<br />
                        Category: $_POST[email_type]<br />
                        Body: $_POST[email_body]</p>";
                } else {
                    echo "It looks like there was a problem sending your email, please try again later.";
                }
            }
            ?>
    </div>
    <hr class="partial_rule" />
    <div id="footer"><span class="top_link"><a href="#header">Link to top</a></span> | Copyright 2013 Ryan Batchelder</div>
</div>
</body>
</html>
