<!DOCTYPE html>
<?php
$check_regex = "/(.com|.org|.gov|.edu|.net)$/"; //Regex to check for the correct email domains
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();

if (!isset($_SESSION[userid])) { //Make sure the user is definitely the admin. Send them back to either the secure page or the login otherwise.
    header('Location: ../index.php');
    if ($_SESSION[userid] != 'admin') //In this case, if they're logged in, but not the admin, send them back to the secure page.
        header('Location: ../secure_index.php');
}
include('database_info.inc');
?>

<head>
    <title>Log Viewer</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Log Viewer</div>
    <hr class="partial_rule" />
    <p id="main_text"><span style="text-decoration: underline;">Username | Time | Type | Account Active</span> <br /> <!--Set up the table headers-->
    <?php
    mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_SESSION[userid]', '$time', 'Log Viewed in Browser')"); //Log that the log was viewed
    $logQuery = "SELECT * FROM logbook"; //Set up the query to the database. For some reason, I had issues putting this query inline like the rest.
    $logContents = mysql_query($logQuery); //Dump the contents of the log into $logContents
    while ($row = mysql_fetch_array($logContents)) { //Go through the arrays of the log and dump them out to the screen
        echo $row[username] . " | " . $row[time] . " | " . $row[type] . " | " . $row[accountActive];
        echo "<br />";
    }
    ?>
        <a href="csvExport.php">Download this log in CSV format</a>
        <a href="../secure_index.php"><-- Back</a>
    </p>
    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>