<!DOCTYPE html>
<?php
$check_regex = "/(.com|.org|.gov|.edu|.net)$/"; //Regex to check for the correct email domains
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();

if (!isset($_SESSION[userid])) {
    header('Location: index.php');
}

$edit_labels = array("email" => "Email Address", "password" => "Password");
$pass_changed = false;
$email_changed = false;
include('database_info.inc');
?>

<head>
    <title>Edit Your Account</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Edit Your Account</div>
    <hr class="partial_rule" />
        <?php
        foreach($_POST as $field => $value) { //Display all of the registration fields
            if ($field == "email") {
                if (!empty($value)) {
                    if (preg_match($check_regex, $value)) { //Catch if they put in an email in the wrong format
                        mysql_query("UPDATE user SET `email` = '" . mysql_real_escape_string($_POST[email]) . "' WHERE username = '" . mysql_real_escape_string($_SESSION[userid]) . "'");
                        mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_SESSION[userid]', '$time', 'Email Change')"); //Log the changed email
                        $email_changed = true;
                    }
                    else { //Catch if the user input an incorrect email address
                        echo "<p id='main_text'>Your email address appears to be invalid, please try again.</p>
                        <form class='login' action='userEditValidator.php' method='post'>
                        <div><label class='loginLabels' for='Email Address'>Email Address: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='45' /></div>
                        <div><input class='submit_left' type='submit' value='Submit Edits' /></div>";
                    }
                }
            }
            else if ($field == "password") {
                if (!empty($value)) {
                    $password = md5($_POST[password]);
                    mysql_query("UPDATE user SET `password` = '" . mysql_real_escape_string($password) . "' WHERE username = '" . mysql_real_escape_string($_SESSION[userid]) . "'");
                    mysql_query("INSERT INTO logbook (username, time, type) VALUES ('$_SESSION[userid]', '$time', 'Password Change')"); //Log the changed password
                    $pass_changed = true;
                }
            }
        }
        if ($email_changed and $pass_changed) {
            $changed_string = "Email Address and Password changed successfully.";
        }
        else if ($email_changed and !$pass_changed) {
            $changed_string = "Email Address changed successfully.";
        }
        else if (!$email_changed and $pass_changed) {
            $changed_string = "Password changed successfully";
        }
        else {
            $changed_string = "Nothing has been changed";
        }

        echo "<p id='main_text'>$changed_string</p>
                <p id='main_text'><a href='../secure_index.php'><-- Back</a></p>"; //It's probably nice to tell the user what got changed.
        ?>
    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>