<!DOCTYPE html>
<?php
session_start();

if (!isset($_SESSION[userid])) {
    header('Location: index.php');
}

$edit_labels = array("email" => "Email Address", "password" => "Password");
?>

<head>
    <title>Edit Your Account</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Please don't leave!</div>
    <hr class="partial_rule" />
    <p id="main_text">Are you sure you wish to delete your account? This process is non-reversible!<br />...and we're going to be really sad :(</p>
    <form class="login" action="deleteAccount.php" method="post">
        <div><input type="submit" value="Yes" /><a href='../userEdit.php'><input type="button" value="No" /></a></div>
    </form>
    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>