<!DOCTYPE html>
<?php
date_default_timezone_set('America/Phoenix');
$time = date('m/d/y H:i');
session_start();
if (isset($_SESSION['username'])) {
    header('Location: secure_index.php');
}
include('database_info.inc');
$users = '';
$user_get = mysql_query("SELECT username FROM user");
while ($row = mysql_fetch_array($user_get)) {
    if ($row[username] != $_SESSION[userid]) {
        if ($row[username] == $_SESSION[reply_to]) {
            $users .= "<option selected>" . $row[username] . "</option>";
        }
        else {
            $users .= "<option>" . $row[username] . "</option>";
        }
    }
}
?>

<head>
    <title>Send Message</title>
    <link rel="stylesheet" type="text/css" href="../styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <?php
        if (isset($_SESSION[reply_to])) { //Set the title based on whether the user is replying or sending a new message
            echo "<div id='header'>Send Reply</div>";
        }
        else {
            echo "<div id='header'>Send Message</div>";
        }
    ?>
    &nbsp;
    <hr class="partial_rule" />

    <div id="main_text">
        <form id="form_space" action="message_validator.php" method="post">
        <?php
        echo "<label for='to'>To: </label><select width='55' class='form_right users' name='to'>" . $users . "</select>";
        echo "<label for='subject'>Subject: </label><input class='form_right' type='text' name='subject' size='55' maxlength='100' value='$_SESSION[reply_subject]' /> ";
        echo "<label for='body'>Body: </label><textarea class='form_right' name='message_body' rows=10 cols=53>Write the body of your message here</textarea> ";
        echo "<div><input class='button_clear' type='submit' value='Send Message' /></div>"; //Submit button
        echo "<div style='clear: both;'><a href='../messaging.php'><-- Go Back</a></div>";
        ?>
        </form>
    </div>

    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>