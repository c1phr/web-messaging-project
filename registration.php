<!DOCTYPE html>
<?php
$reg_labels = array("email" => "Email Address", "userid" => "UserID", "password" => "Password");
session_start();

?>

    <head>
    <title>Ryan Batchelder Registration</title>
    <link rel="stylesheet" type="text/css" href="styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
    </head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <div id="header">Ryan Batchelder</div>
    &nbsp;
    <hr class="partial_rule" />
    <p id="main_text">Register to access the rest of the site. Please enter your desired username and password.</p>
    <form class="login" action="php/registrationValidator.php" method="post">
        <?php
            foreach($reg_labels as $field => $label) { //Display all of the registration fields
                if ($field == "email")
                    echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='45' /></div>";
                else if ($field == "userid")
                    echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='text' name='$field' id='$field' size='30' maxlength='100' /></div>";
                else if ($field == "password")
                    echo "<div><label class='loginLabels' for='$label'>$label: </label><input class='login_right' type='password' name='$field' id='$field' size='30' maxlength='100' /></div>";
            }
        ?>
        <div><input class="submit_left" type="submit" value="Login" /></div>
    </form>
    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>