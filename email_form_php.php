<!DOCTYPE html>
<?php
session_start();

if (!isset($_SESSION[userid])) {
    header('Location: index.php');
}
?>
<head>
    <?php
    $form_labels = array("name" => "Name", "email" => "Email Address", "subject" => "Email Subject", "type" => "Email Type", "body" => "Email Body"); //Array of form elements
    ?>
	<title>Email Ryan</title>
	<link rel="stylesheet" type="text/css" href="styles/base_styles.css">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
	<div id="container">
		<div id="header">Email Ryan</div>
		<div>
			<ul id="nav">
			<li><a href="index.html" class="left_end">Project 1</a></li>
			<li><a href="page2.html">Project 2</a></li>
            <li><a href="page3.html">Project 3</a></li>
            <li><a href="email_form_php.php" class="right_end">Project 5/6</a></li>
			</ul>
		</div>
	<hr class="partial_rule" />
		<div id="main_text">
			<form id="form_space" action="php/emailValidator.php" method="post">
                <?php
                    foreach ($form_labels as $field => $label) { //Iterate through and echo out the labels of the form elements
                        echo "<div><label for='$label'>$label: </label> ";
                        if ($field == "name" or $field == "email" or $field == "subject") { //Inputs for name, email and subject
                            echo "<input class='form_right' type='text' name='$field' id='$field' size='55' maxlength='100' /> ";
                        }
                        elseif ($field == "type") { //Handle the radio buttons
                            echo "<div class='radio_fix form_right'><select name='email_type'>
                                            <option value='Urgent'>Urgent</option>
                                            <option value='Question'>Question</option>
                                            <option value='Comment'>Comment</option>
                                            </select><br />";

                            //The following is the old code that managed radio buttons instead of a dropdown.

                            /*echo "<div class='radio_fix'><span class='radio_space'>Urgent:<input type='radio' value='Urgent' name='group1' /></span>
                                  <span class='radio_space'>Question:<input type='radio' value='Question' name='group1' /></span>
                                  Comment:<input type='radio' value='Comment' name='group1' /><br />";*/
                        }
                        elseif ($field == "body") { //The final item is the email body text area
                            echo "<textarea class='form_right' name='email_body' rows=10 cols=50>Write the body of your message here</textarea>";
                        }

                        echo "</div>"; //Close the div for each label/input pair

                    }
                echo "<div><input class='button_clear' type='submit' value='Send Email' /></div>"; //Submit button
                ?>
			</form>
		</div>
	<hr class="partial_rule" />
		<div id="footer"><span class="top_link"><a href="#header">Link to top</a></span> | Copyright 2013 Ryan Batchelder</div>
	</div>
</body>
</html>
