<!DOCTYPE html>
<?php
session_start();

if ($_SESSION[newAcc]){
    $newAcc = true;
    unset($_SESSION[newAcc]);
}


if (!isset($_SESSION[userid])) {
    header('Location: index.php');
}
?>

<head>
    <title>Ryan Batchelder Secure Area</title>
    <link rel="stylesheet" type="text/css" href="styles/base_styles.css"> <!-- Link stylesheet -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'> <!-- Add a nicer font from Google Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Molengo' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container"> <!-- Open the container Div, almost everything is going to go in here -->
    <?php
    if ($newAcc)
        echo "<div id='header'>Registration successful!</div>";
    else
        echo "<div id='header'>Welcome Back!</div>";
    ?>
    &nbsp;
    <hr class="partial_rule" />
    <div id="main_text">
        <p>Welcome to the secure area, <?php echo "$_SESSION[userid]"; ?>!</p>
        <p><a href="email_form_php.php">Email Ryan</a></p>
        <p><a href="messaging.php">Enter the Messaging System</a></p>
        <p><a href="userEdit.php">Edit my account</a></p>
        <?php
        if ($_SESSION[userid] == 'admin') //We only want the admin to be able to see the database logs.
            echo "<p><a href='php/logViewer.php'>Click here to view database logs</a></p>";
        ?>
        <p><a href="php/logout.php">Log me out!</a></p>
    </div>

    <hr class="partial_rule" />
    <div id="footer">Copyright 2013 Ryan Batchelder</div>
</div> <!-- Close the container, we're done with the page now -->
</body>